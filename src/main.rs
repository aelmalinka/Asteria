//! Entrypoint for Asteria
//!
//! Builds bevy app with desired plugins and runs it
#![warn(
    clippy::pedantic,
    clippy::nursery,
    missing_docs,
    missing_debug_implementations
)]
#![deny(unsafe_code)]
use asteria::prelude::*;
use bevy::prelude::*;
use bevy::window::{WindowMode, WindowTheme};

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: "Asteria".into(),
                        name: Some("asteria".into()),
                        window_theme: Some(WindowTheme::Dark),
                        mode: WindowMode::BorderlessFullscreen,
                        ..default()
                    }),
                    ..default()
                })
                .set(ImagePlugin::default_nearest()),
        )
        .add_plugins(Asteria)
        .run();
}
