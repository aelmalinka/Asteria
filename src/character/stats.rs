use hecate::prelude::*;

#[stat(cost = 8)]
pub struct Strength(Percentage);
#[stat(cost = 8)]
pub struct Agility(Percentage);
#[stat(cost = 2)]
pub struct Endurance(Percentage);
#[stat(cost = 1)]
pub struct Perception(Percentage);
#[stat(cost = 3)]
pub struct Magic(Percentage);
#[stat(cost = 1)]
pub struct Willpower(Percentage);

#[stat(cost = 2)]
pub struct Melee(Percentage);
#[stat(cost = 1)]
pub struct Sword(Percentage);
#[stat(cost = 1)]
pub struct Blunt(Percentage);
#[stat(cost = 1)]
pub struct Unarmed(Percentage);

#[stat(cost = 1)]
pub struct Ranged(Percentage);
#[stat(cost = 1)]
pub struct Bow(Percentage);
#[stat(cost = 1)]
pub struct Thrown(Percentage);

#[stat(cost = 1)]
pub struct Unarmored(Percentage);
#[stat(cost = 1)]
pub struct LightArmor(Percentage);
#[stat(cost = 1)]
pub struct HeavyArmor(Percentage);

#[stat(cost = 1)]
pub struct Divination(Percentage);
#[stat(cost = 1)]
pub struct Alteration(Percentage);
#[stat(cost = 1)]
pub struct Healing(Percentage);
#[stat(cost = 1)]
pub struct Destruction(Percentage);

#[stat(cost = 1)]
pub struct Find(Percentage);

#[resource(cost = 1, transform = 10)]
pub struct Health(resource::Raw);
