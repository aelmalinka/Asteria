use hecate::prelude::*;

pub use super::stats::*;

#[character]
pub struct Character {
    #[ignore]
    name: String,

    strength: Strength,
    agility: Agility,
    endurance: Endurance,
    perception: Perception,
    magic: Magic,
    willpower: Willpower,

    #[skill(strength, strength, agility)]
    melee: Melee,
    #[skill(melee)]
    sword: Sword,
    #[skill(melee)]
    blunt: Blunt,
    #[skill(melee)]
    unarmed: Unarmed,

    #[skill(strength, strength, agility)]
    ranged: Ranged,
    #[skill(ranged)]
    bow: Bow,
    #[skill(ranged)]
    thrown: Thrown,

    #[skill(agility, agility, endurance)]
    unarmored: Unarmored,
    #[skill(unarmored, strength)]
    light_armor: LightArmor,
    #[skill(unarmored, strength, endurance)]
    heavy_armor: HeavyArmor,

    #[skill(magic, perception)]
    divination: Divination,
    #[skill(magic, willpower)]
    alteration: Alteration,
    #[skill(magic, endurance)]
    healing: Healing,
    #[skill(magic, strength)]
    destruction: Destruction,

    #[skill(perception, perception, divination)]
    find: Find,

    #[skill(endurance)]
    health: Health,
}
