//! Hecate stats/character types/impls

mod stats;
mod structs;

pub use structs::Character;
