//! Game logic and plugins
#![warn(
    clippy::pedantic,
    clippy::nursery,
    missing_docs,
    missing_debug_implementations
)]
// note: seems this is pattern used by bevy todo?
#![allow(clippy::needless_pass_by_value)]
#![deny(unsafe_code)]

pub mod character;
pub mod plugin;
pub mod prelude;
