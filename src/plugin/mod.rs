//! Main plugin for bevy/asteria integration
mod sprite;

use bevy::prelude::*;

use crate::character::Character;

/// The actual plugin
#[derive(Clone, Copy, Debug)]
pub struct Asteria;

impl Plugin for Asteria {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, spawn_player)
            .add_plugins(sprite::Plugin)
            .add_systems(PreUpdate, input);
    }
}

#[derive(Component)]
struct Player;

#[derive(Component)]
struct Enemy;

fn input(
    input: Res<ButtonInput<KeyCode>>,
    mut query: Query<(&mut sprite::Facing, &mut sprite::Movement), With<Player>>,
) {
    let (mut facing, mut movement) = query.single_mut();

    if input.just_pressed(KeyCode::ArrowLeft) {
        *facing = sprite::Facing::West;
        **movement = true;
    } else if input.just_pressed(KeyCode::ArrowRight) {
        *facing = sprite::Facing::East;
        **movement = true;
    } else if input.just_pressed(KeyCode::ArrowUp) {
        *facing = sprite::Facing::North;
        **movement = true;
    } else if input.just_pressed(KeyCode::ArrowDown) {
        *facing = sprite::Facing::South;
        **movement = true;
    } else if input.any_just_released([
        KeyCode::ArrowLeft,
        KeyCode::ArrowRight,
        KeyCode::ArrowUp,
        KeyCode::ArrowDown,
    ]) {
        **movement = false;
    }
}

fn spawn_player(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
    commands.spawn((Player, Character::default(), sprite::Moving {
        path: "Malinka.png".into(),
        zorder: 10.0.into(),
        ..default()
    }));
    commands.spawn((Enemy, sprite::Moving {
        path: "Orc.png".into(),
        position: (1.0, 1.0).into(),
        ..default()
    }));
}
