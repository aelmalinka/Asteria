use std::ops::Mul;

use bevy::prelude::*;
use derive_more::{AsRef, From};
use num_enum::IntoPrimitive;

pub struct Plugin;

impl ::bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Animation::default())
            .insert_resource(Positioning::default())
            .add_systems(PostStartup, load_sprites)
            .add_systems(
                Update,
                (position_change, facing_change, position, animation),
            );
    }
}

#[derive(Default, Deref, DerefMut, Component, AsRef, From)]
#[from(forward)]
pub struct Path(String);

#[derive(Copy, Clone, Default, Component, From)]
pub struct Position(pub f32, pub f32);

#[derive(Copy, Clone, Default, Deref, DerefMut, Component, AsRef, From)]
pub struct ZOrder(f32);

#[derive(Resource, Deref, DerefMut)]
pub struct Animation(Timer);

#[derive(Resource, Deref, DerefMut)]
pub struct Positioning(Timer);

impl Default for Animation {
    fn default() -> Self {
        Self(Timer::from_seconds(0.3, TimerMode::Repeating))
    }
}

impl Default for Positioning {
    fn default() -> Self {
        Self(Timer::from_seconds(0.1, TimerMode::Repeating))
    }
}

#[derive(Copy, Clone, Default, Deref, DerefMut, Component, AsRef, From)]
pub struct Movement(bool);

#[derive(Default, Bundle)]
pub struct Moving {
    pub path: Path,
    pub facing: Facing,
    pub position: Position,
    pub transform: TransformBundle,
    pub movement: Movement,
    pub zorder: ZOrder,
}

pub const MOVING_WIDTH: f32 = 32.0;
pub const MOVING_HEIGHT: f32 = 48.0;

#[derive(Copy, Clone, Default, Debug, PartialEq, Eq, Component, IntoPrimitive)]
#[repr(u8)]
pub enum Facing {
    #[default]
    South = 0,
    West = 1,
    East = 2,
    North = 3,
}

impl Mul<u8> for Facing {
    type Output = u8;

    fn mul(self, o: u8) -> Self::Output {
        <Self as Into<u8>>::into(self) * o
    }
}

const FRAMES: u8 = 4;

fn position_change(
    time: Res<Time>,
    mut query: Query<(&mut Position, &Facing, &Movement)>,
    mut timer: ResMut<Positioning>,
) {
    if timer.tick(time.delta()).just_finished() {
        for (mut position, &facing, &movement) in &mut query {
            if *movement {
                match facing {
                    Facing::East => {
                        position.0 += 1.0;
                    }
                    Facing::West => {
                        position.0 -= 1.0;
                    }
                    Facing::North => {
                        position.1 += 1.0;
                    }
                    Facing::South => {
                        position.1 -= 1.0;
                    }
                }
            }
        }
    }
}

fn facing_change(mut query: Query<(&mut TextureAtlas, &Facing), Changed<Facing>>) {
    for (mut sprite, &facing) in &mut query {
        sprite.index = (facing * FRAMES).into();
    }
}

// 2024-04-25 MLT TODO: changes to zorder
fn position(mut query: Query<(&mut Transform, &Position, &ZOrder), Changed<Position>>) {
    for (mut sprite, position, &zorder) in &mut query {
        *sprite = Transform::from_xyz(
            position.0 * MOVING_WIDTH,
            position.1 * MOVING_HEIGHT,
            *zorder,
        );
    }
}

fn animation(
    time: Res<Time>,
    mut query: Query<(&mut TextureAtlas, &Facing)>,
    mut timer: ResMut<Animation>,
) {
    if timer.tick(time.delta()).just_finished() {
        for (mut sprite, &facing) in &mut query {
            sprite.index =
                ((sprite.index + 1) % usize::from(FRAMES)) + usize::from(facing * FRAMES);
        }
    }
}

fn load_sprites(
    mut commands: Commands,
    assets: Res<AssetServer>,
    mut atlases: ResMut<Assets<TextureAtlasLayout>>,
    query: Query<(Entity, &Path)>,
) {
    for (entity, path) in &query {
        let texture = assets.load(path.as_ref());
        let layout = atlases.add(TextureAtlasLayout::from_grid(
            Vec2::new(MOVING_WIDTH, MOVING_HEIGHT),
            4,
            4,
            None,
            None,
        ));
        commands.entity(entity).insert(SpriteSheetBundle {
            texture,
            atlas: TextureAtlas { layout, index: 1 },
            ..default()
        });
    }
}
